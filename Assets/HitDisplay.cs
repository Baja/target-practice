﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitDisplay : MonoBehaviour
{
    [SerializeField] private GameObject hitMarkerPrefab;

    public void CreateHit(Vector3 hitLocation)
    {
        GameObject go = Instantiate(hitMarkerPrefab, this.gameObject.transform);
        go.transform.localPosition = hitLocation;
    }
}
