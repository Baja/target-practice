﻿using Boilerplate.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool Instance;
    public List<List<GameObject>> pools = new List<List<GameObject>>();
    private bool poolExists = false;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject GetOrCreatePool(GameObject prefab, int maxSize)
    {
        if(pools.Count <= 0)
            CreateNewPool(prefab, maxSize);
        foreach (List<GameObject> pool in pools)
        {
            if (pool.Contains(prefab))
            {
                poolExists = true;
                return GetPooledEffect(pool);
            }
            else
            {
                CreateNewPool(prefab, maxSize);
                return GetOrCreatePool(prefab, maxSize);
            }
        }

        return null;
    }

    void CreateNewPool(GameObject prefab, int maxSize)
    {
        var _pooledObjects = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < maxSize; i++)
        {
            tmp = Instantiate(prefab);
            tmp.SetActive(false);
            _pooledObjects.Add(tmp);
        }
        pools.Add(_pooledObjects);
    }

    public GameObject GetPooledEffect(List<GameObject> pooledObjects)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
            if (!pooledObjects[0].activeInHierarchy)
                return pooledObjects[0];
        return null;
    }

    public void DisableAllEffects()
    {
        foreach (List<GameObject> pool in pools)
            foreach (GameObject go in pool)
                go.SetActive(false);
    }
}
