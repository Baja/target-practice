﻿using UnityEngine;

namespace Boilerplate.Utils
{
    public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        public static T Instance { get => instance; private set => instance = value; }
        private static T instance;

        public bool MakeDontDestroyOnLoad = true;

        protected virtual void Awake()
        {
            if (instance != null)
            {
                Debug.LogError($"An instance of {this.name} already exists");
                Destroy(this.gameObject);
                return;
            }

            instance = (T)this;

            if (MakeDontDestroyOnLoad)
            {
                DontDestroyOnLoad(this.gameObject);
            }
        }
    }
}
