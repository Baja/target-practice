﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using TMPro;
using UnityEngine.XR;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    public bool FPS = false;
    [SerializeField] private float _fireWait = 0.1f;
    [SerializeField] private ParticleSystem cartridgePrefab;
    [SerializeField] private AudioClip _headshotAudioClip;
    [SerializeField] private AudioClip _gunshotAudioClip;
    [SerializeField] private GameObject _headshotTarget;
    [SerializeField] private GameObject[] _targets;
    [SerializeField] private TrailRenderer _tracer;
    [SerializeField] private AudioSource _headshotAudioSource;
    [SerializeField] private AudioSource _NormalAudioSource;
    [SerializeField] private Transform _barrelTransform;
    [SerializeField] private Weapon weapon = null;
    [SerializeField] private TextMeshProUGUI _accuracyText;
    [SerializeField] private TextMeshProUGUI _totalHeadshotsText;
    [SerializeField] private HitDisplay _hitDisplayObject;
    private int ProjectilesFired;
    private int hits;
    
    
    private GameObject _activeTarget = null;
    private GameObject _previouslyActiveTarget = null;
    private ParticleSystem _muzzleFlashParticleEffect;
    private Coroutine firingRoutine = null;
    private WaitForSeconds wait = null;
    
    private void Awake()
    {
        wait = new WaitForSeconds(_fireWait);
        _muzzleFlashParticleEffect = _barrelTransform.gameObject.GetComponentInChildren<ParticleSystem>();
        _activeTarget = _targets[Random.Range(0, _targets.Length)];
        _activeTarget.SetActive(true);
        _previouslyActiveTarget = _activeTarget;
    }
    
    public void Setup(Weapon weapon)
    {
        weapon.onSelectExit.AddListener(StopFiring);
    }



    private void OnDestroy()
    {
        weapon.onSelectExit.RemoveListener(StopFiring);
    }

    public void StartFiring(XRBaseInteractor interactor)
    {
        firingRoutine = StartCoroutine(FiringSequence());
    }

    private IEnumerator FiringSequence()
    {
        while (gameObject.activeSelf)
        {
            CreateProjectile();
            weapon.ApplyRecoil();
            yield return wait;
        }
    }

//     private void Update()
//     {
//         if (Input.GetMouseButtonDown(1))
//         {
//             CreateProjectile();
//         }
// }

    private void CreateProjectile()
    {
        _muzzleFlashParticleEffect.Play();
        cartridgePrefab.Play();
        CastRayForShot(Instantiate(_tracer));
    }
    
    private void CastRayForShot(TrailRenderer tracerRound)
    {
        if (FPS)
        {
            UpdateFiredCount(ProjectilesFired + 1);
            GameObject pooledHitParticleEffect = HitPool.Instance.GetPooledHitEffect();
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 300))
            {
                CheckHitValue(hit, pooledHitParticleEffect);
                weapon._canFire = false;
                MoveTracerToBarrel(tracerRound, hit);
            }
        }
        else
        {
            UpdateFiredCount(ProjectilesFired + 1);
            GameObject pooledHitParticleEffect = HitPool.Instance.GetPooledHitEffect();
            RaycastHit hit;
            bool raycastDidHit = Physics.Raycast(_barrelTransform.position, _barrelTransform.forward, out hit, 100);
            MoveTracerToBarrel(tracerRound, hit);
            weapon._canFire = false;
            if (raycastDidHit)
                CheckHitValue(hit, pooledHitParticleEffect);
        }

    }

    private void MoveTracerToBarrel(TrailRenderer tracerRound, RaycastHit hit)
    {
        tracerRound.AddPosition(_barrelTransform.position);
        tracerRound.transform.position = hit.point;
        tracerRound.transform.forward = hit.normal;
        StartCoroutine(StopTracer(tracerRound.gameObject));
    }

    IEnumerator StopTracer(GameObject tracerRound)
    {
        yield return new WaitForSeconds(.05f);
        Destroy(tracerRound);
    }

    private void CheckHitValue(RaycastHit hit, GameObject hitParticleEffect)
    {

        hitParticleEffect.gameObject.SetActive(true);
        _NormalAudioSource.PlayOneShot(_gunshotAudioClip);
        PlayHitParticleSystem(hitParticleEffect, hit);
    }



    private void ShuffleNextTarget()
    {
        _activeTarget = _targets[Random.Range(0, _targets.Length - 1)];
        if (_activeTarget == _previouslyActiveTarget)
            ShuffleNextTarget();
        else
            _activeTarget.SetActive(true);
    }
    private void PlayHitParticleSystem(GameObject particleSystemToPlay, RaycastHit hit)
    {
        ParticleSystem particleSystem = particleSystemToPlay.GetComponent<ParticleSystem>();
        if (hit.transform.gameObject.name == _headshotTarget.name)
        {
            particleSystemToPlay.transform.parent = hit.transform;
            particleSystemToPlay.transform.position = hit.point;
            particleSystemToPlay.transform.forward = hit.normal;
            HandleHeadshot(particleSystemToPlay.transform.localPosition);
            particleSystem.Play();
        }
        else
        {
            particleSystemToPlay.transform.position = hit.point;
            particleSystemToPlay.transform.forward = hit.normal;
            particleSystem.Play();     
        }

        StartCoroutine(DisableParticleEffectForSuccessfulHit(particleSystemToPlay, particleSystem.main.duration));
    }
    
    private void HandleHeadshot(Vector3 hitLocation )
    {
        Debug.Log(hitLocation);
        hits++;
        _headshotAudioSource.PlayOneShot(_headshotAudioClip);
        _activeTarget.SetActive(false); 
        _hitDisplayObject.CreateHit(hitLocation);
        ShuffleNextTarget();
    }

    public void StopFiring(XRBaseInteractor interactor)
    {
        if (firingRoutine != null)
            StopCoroutine(firingRoutine);
    }

    IEnumerator DisableParticleEffectForSuccessfulHit(GameObject go, float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
        go.SetActive(false);
    }
    
    public void UpdateFiredCount(int newValue)
    {
        ProjectilesFired = newValue;
        SetStringValues((float)newValue, hits);
    }

    private void SetStringValues(float value, float hit)
    {
        float avgHits = (float)(hit / value);
        int acc = Mathf.RoundToInt(avgHits * 100);
        _accuracyText.text = "Accuracy " + acc.ToString() + "%";
        _totalHeadshotsText.text = "Hits " + hits.ToString();
    }
}
