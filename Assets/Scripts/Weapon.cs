﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Weapon : XRGrabInteractable
{

    [SerializeField] private GameController _gameManager;
    private GripHold gripHold = null;
    private GuardHold guardHold = null;
    private new Rigidbody rigidbody = null;
    private XRBaseInteractor gripHand = null;
    private XRBaseInteractor guardHand = null;
    private readonly Vector3 gripRotation = new Vector3(45, 0, 0);
    private float _handBreakDistance = 0.25f;
    private int _recoilAmount = 5;
    private int _maxProjectiles = 30;
    public bool _canFire = true;
    

    protected override void Awake()
    {
        base.Awake();
        SetupHolds();
        SetupExtras();
        onSelectEnter.AddListener(SetInitialRotation);
    }

    private void SetupHolds()
    {
        gripHold = GetComponentInChildren<GripHold>();
        gripHold.Setup(this);

        guardHold = GetComponentInChildren<GuardHold>();
        guardHold.Setup(this);
    }

    private void SetupExtras()
    {
        rigidbody = GetComponent<Rigidbody>();

    }

    private void OnDestroy()
    {
        onSelectEnter.RemoveListener(SetInitialRotation);
    }

    private void SetInitialRotation(XRBaseInteractor interactor)
    {
        Quaternion newRotation = Quaternion.Euler(gripRotation);
        interactor.attachTransform.localRotation = newRotation;
    }

    public void SetGripHand(XRBaseInteractor interactor)
    {
        gripHand = interactor;
        OnSelectEnter(gripHand);
        _gameManager.Setup(this);     
    }

    public void ClearGripHand(XRBaseInteractor interactor)
    {
        gripHand = null;
        OnSelectExit(interactor);
    }

    public void SetGuardHand(XRBaseInteractor interactor)
    {
        guardHand = interactor;
    }

    public void ClearGuardHand(XRBaseInteractor interactor)
    {
        guardHand = null;
    }

    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        base.ProcessInteractable(updatePhase);

        if (updatePhase == XRInteractionUpdateOrder.UpdatePhase.Dynamic)
        {
            if (gripHand && guardHand)
                SetGripRotation();

            CheckDistance(gripHand, gripHold);
            CheckDistance(guardHand, guardHold);
        }
    }

    private void SetGripRotation()
    {
        Vector3 target = guardHand.transform.position - gripHand.transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(target);
        Vector3 gripRotation = Vector3.zero;
        gripRotation.z = gripHand.transform.eulerAngles.z;
        lookRotation *= Quaternion.Euler(gripRotation);
        gripHand.attachTransform.rotation = lookRotation;
    }

    private void CheckDistance(XRBaseInteractor interactor, HandHold handHold)
    {
        if (interactor)
        {
            float distanceSqr = GetDistanceSqrToInteractor(interactor);
            if (distanceSqr > _handBreakDistance)
                handHold.BreakHold(interactor);
        }
    }

    public void PullTrigger()
    {
        if (_canFire)
        { 
            _gameManager.StartFiring(gripHand);
            _canFire = false;
        }
    }

    public void ReleaseTrigger()
    {
        _gameManager.StopFiring(gripHand);
        _canFire = true;
    }

    public void ApplyRecoil()
    {
        rigidbody.AddRelativeForce(Vector3.back * _recoilAmount, ForceMode.Impulse);
    }
}
