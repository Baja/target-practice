﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPool : MonoBehaviour
{
    public static HitPool Instance;
    [SerializeField] List<GameObject> _pooledEffects;
    [SerializeField] GameObject _effectPrefab;
    [SerializeField] int _maxEffects;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _pooledEffects = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < _maxEffects; i++)
        {
            tmp = Instantiate(_effectPrefab);
            tmp.SetActive(false);
            _pooledEffects.Add(tmp);
        }
    }

    public GameObject GetPooledHitEffect()
    {
        for (int i = 0; i < _maxEffects; i++)
            if (!_pooledEffects[i].activeInHierarchy)
                return _pooledEffects[i];
        return null;
    }

    public void DisableAllEffects()
    {
        foreach (GameObject effect in _pooledEffects)
            effect.SetActive(false);
    }
}