﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _audioClip;
    [SerializeField] private float _lifetime = 0.8f;

    private void Start()
    {
        PlayShot();
    }

    public void PlayShot()
    {
        _audioSource.PlayOneShot(_audioClip);
        StartCoroutine(Deactivate());
    }

    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(_lifetime);
        gameObject.SetActive(false);
    }


}
