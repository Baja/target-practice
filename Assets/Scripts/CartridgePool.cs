﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartridgePool : MonoBehaviour
{
    public static CartridgePool Instance;
    [SerializeField] List<GameObject> _pooledEffects;
    [SerializeField] GameObject _effectPrefab;
    [SerializeField] Transform _effectParent;
    [SerializeField] int _maxEffects;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _pooledEffects = new List<GameObject>();
        GameObject tmp;
        for (int i = 0; i < _maxEffects; i++)
        {
            tmp = Instantiate(_effectPrefab, _effectParent);
            tmp.SetActive(false);
            _pooledEffects.Add(tmp);
        }
    }

    public GameObject GetPooledCartridgeEffect()
    {
        for (int i = 0; i < _maxEffects; i++)
            if (!_pooledEffects[i].activeInHierarchy)
                return _pooledEffects[i];
        return null;
    }

    public void DisableAllEffects()
    {
        foreach (GameObject effect in _pooledEffects)
            effect.SetActive(false);
    }
}
